@extends('layouts.app-master')

@section('content')
@auth
@include('siswa.sidebar')
<div class="col-sm-9">
    <div class="row gy-4">
        <div class="col-sm-12">
            <div class="card sm-12">
                <div class="card-body">
                    <div class="card">
                        <h5 class="card-header">Seleksi</h5>
                        <div class="table-responsive text-nowrap">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Tes Akademik:</th>
                                        <td>
                                            <div class="col-sm-4">
                                                @if($user->statusTesAkademik == 2)
                                                    <span class="text-danger">Gagal</span>
                                                @elseif($user->statusTesAkademik == 3)
                                                    <span class="text-success">Lulus</span>
                                                @elseif($user->statusTesPsikotest == 1)
                                                    <span class="text-detail">Belum mengikuti Test Akademik</span>
                                                @else
                                                    <a href=""><button class="btn btn-primary">Hasil test</button></a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Tes Psikotest:</th>
                                        <td>
                                            <div class="col-sm-4">
                                                @if($user->statusTesPsikotest == 2)
                                                    <span class="text-danger">Gagal</span>
                                                @elseif($user->statusTesPsikotest == 3)
                                                    <span class="text-success">Lulus</span>
                                                @elseif($user->statusTesPsikotest == 1)
                                                    <span class="text-detail">Belum mengikuti Psikotest</span>
                                                @else
                                                    <a href=""><button class="btn btn-primary">Hasil Test</button></a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>
@endauth
@endsection
