@extends('layouts.app-master')


@section('content')
@auth
@include('siswa.sidebar')

<div class="pagetitle">
<h1><b>Tes Akademik</b></h1>
<div class="col-sm-8">
    <div class="card">
      <div class="card-header">
        <div class="d-flex align-items-center justify-content-between">
           
          <div class="row"><form action="{{ route('tesAkademik.submit') }}" method="post">
          <h5 class="card-title m-0 me-2">Daftar Soal</h5> 
            @csrf
            @foreach($data as $soal)
                <div class="mb-3">
                    <br>
                    <p>{{ $soal->deskripsi_soal }}</p>
                    <input type="hidden" name="soal_id[]" value="{{ $soal->id }}">
                    <textarea name="jawaban[]"></textarea>
                </div>
            @endforeach
            <button type="submit" class="btn btn-primary">Kirim Jawaban</button>
        </form>     
        </div>
       </div>
      </div>
    </div>
</div>
  @endauth
@endsection