@extends('layouts.app-master')

@section('content')
@auth
@include('siswa.sidebar')
<main id="main" class="main">

    <div class="pagetitle">
        <h1><b>Data Calon Siswa</b></h1>
        <nav>
        </nav>
    </div>

    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Calon Siswa</h5>
                            <form action="{{ route('akunSiswa.store') }}" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group">
                                    <label class="font-weight-bold">Email</label>
                                    <input type="text" class="form-control @error('email') is-invalid @enderror" name="email">
                                    @error('email')
                                    <div class=" alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">NISN</label>
                                    <input type="number" class="form-control @error('nisn') is-invalid @enderror" name="nisn">
                                    @error('nisn')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Lengkap</label>
                                    <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap">
                                    @error('nama_lengkap')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Jenis Kelamin</label>
                                    <select class="form-control @error('jenis_kelamin') is-invalid @enderror" name="jenis_kelamin">
                                        <option value="Laki-laki">Laki-Laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                                                  
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Tempat Tanggal Lahir</label>
                                    <input type="text" class="form-control @error('tempat+_tanggal_lahir') is-invalid @enderror" name="tempat_tanggal_lahir">
                                    @error('password')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Agama</label>
                                    <input type="text" class="form-control @error('agama') is-invalid @enderror" name="agama">
                                    @error('agama')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Alamat</label>
                                    <input type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat">
                                    @error('alamat')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Anak Ke-</label>
                                    <input type="text" class="form-control @error('anak_ke') is-invalid @enderror" name="anak_ke">
                                    @error('anak_ke')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Sekolah Asal</label>
                                    <input type="text" class="form-control @error('sekolah_asal') is-invalid @enderror" name="sekolah_asal">
                                    @error('sekolah_asal')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Nama Orangtua</label>
                                    <input type="text" class="form-control @error('nama_orangtua') is-invalid @enderror" name="nama_orangtua">
                                    @error('nama_orangtua')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">No HP Orangtua</label>
                                    <input type="number" class="form-control @error('no_hp_orangtua') is-invalid @enderror" name="no_hp_orangtua">
                                    @error('no_hp_orangtua')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Kartu Keluarga</label>
                                    <input type="file" class="form-control @error('kartu_keluarga') is-invalid @enderror" name="kartu_keluarga">
                                    @error('kartu_keluarga')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Akta Kelahiran</label>
                                    <input type="file" class="form-control @error('akta_kelahiran') is-invalid @enderror" name="akta_kelahiran">
                                    @error('akta_kelahiran')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Ijazah Terakhir</label>
                                    <input type="file" class="form-control @error('ijazah_terakhir') is-invalid @enderror" name="ijazah_terakhir">
                                    @error('ijazah_terakhir')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Raport</label>
                                    <input type="file" class="form-control @error('raport') is-invalid @enderror" name="raport">
                                    @error('raport')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Pas Photo</label>
                                    <input type="file" class="form-control @error('pas_photo') is-invalid @enderror" name="pas_photo">
                                    @error('pas_photo')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div hidden class="form-group">
                                    <label class="font-weight-bold">Status Penerimaan</label>
                                    <input type="text" class="form-control @error('status_penerimaan') is-invalid @enderror" name="status_penerimaan">
                                    @error('status_penerimaan')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <label class="font-weight-bold">Tanggal Penerimaan</label>
                                    <input type="date" class="form-control @error('tanggal_penerimaan') is-invalid @enderror" name="tanggal_penerimaan">
                                    @error('tanggal_penerimaan')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <br>

                                <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                                <button type="reset" class="btn btn-md btn-warning">HAPUS</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min. js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap .min.js"></script>
        <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>

        @endauth
        @endsection