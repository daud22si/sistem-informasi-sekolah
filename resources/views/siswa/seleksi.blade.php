@extends('layouts.app-master')


@section('content')
@auth
@include('siswa.sidebar')
<div class="col-sm-9">
<div class="row gy-4">
    <div class="row">
        <!-- Basic Layout -->
        <!-- <div class="col-sm-2"></div> -->

        <div class="col-sm-12">
            <div class="card sm-12">

                <div class="card-body">
                    <!-- Basic Bootstrap Table -->
                    <div class="card">
                        <h5 class="card-header" style="">Seleksi</h5>
                        <div class="table-responsive text-nowrap">
                            <table class="table">
                                <thead>
                                    <hr style="font-weight: bold;">
                                    <hr style="font-weight: bold;">
                                    <tr style="font-weight: bold;">
                                        <th>Tes Akademik:</th>
                                        <td><div class="col-sm-4">
                                            <a href="{{route('tesAkademik')}}"> <button class="btn btn-primary">Mulai Test</button></a>
                                        </div></td>
                                    </tr>
                                    <tr style="font-weight: bold; margin-left: -200px;">
                                        <th>Tes Psikotest: </th>
                                        <td><div class="col-sm-4">
                                            <a href="{{route('tesPsikotest')}}"> <button class="btn btn-primary">Mulai Test</button></a>
                                        </div></td>
                                    </tr>

                            </table>
                            <hr style="font-weight: bold;">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>
@endauth
@endsection