@extends('layouts.app-master')


@section('content')
@auth
@include('siswa.sidebar')
<div class="col-sm-9">
<div class="row gy-4">
    <div class="row">
        <!-- Basic Layout -->
        <!-- <div class="col-sm-2"></div> -->

        <div class="col-sm-12">
            <div class="card sm-12">

                <div class="card-body">
                    <!-- Basic Bootstrap Table -->
                    <div class="card">
                        <h5 class="card-header" style="">Verifikasi</h5>
                        <div class="table-responsive text-nowrap">
                            <table class="table">
                                <thead>
                                    <hr style="font-weight: bold;">
                                    <hr style="font-weight: bold;">
                                    <tr style="font-weight: bold;">
                                        <th>Nama Lengkap :</th>
                                        <td>{{auth()->user()->username}}</td>
                                    </tr>
                                    <td>
                                        @if($user->status == 1)
                                            <span class="text-danger">Anda belum mengisi Form</span>
                                        @elseif($user->status == 2)
                                            <span class="text-success">Menunggu Verifikasi</span>
                                        @elseif($user->status == 3)
                                            <span class="text-danger">Data Anda tidak sesuai, mohon kirim kembali data dibawah ini</span>
                                            <form action="{{ route('akunSiswa.update',$user->id) }}" method="post" enctype="multipart/form-data">
                                            @foreach($data as $siswa)
                                                @csrf
                                                @method('PUT')

                                                <br>
                                                
                                                @if($siswa->kartu_keluarga == 'tidak sesuai')
                                                <div class="form-group">
                                                    <label class="font-weight-bold">Kartu Keluarga</label>
                                                    <input type="file" class="form-control" name="kartu_keluarga">
                                                </div>
                                                @endif
                                                @if($siswa->akta_kelahiran == 'tidak sesuai')
                                                <br>
                                                <div class="form-group">
                                                    <label class="font-weight-bold">Akta Kelahiran</label>
                                                    <input type="file" class="form-control" name="akta_kelahiran">
                                                </div>
                                                @endif
                                                @if($siswa->ijazah_terakhir == 'tidak sesuai')
                                                <br>
                                                <div class="form-group">
                                                    <label class="font-weight-bold">Ijazah Terakhir</label>
                                                    <input type="file" class="form-control" name="ijazah_terakhir">
                                                </div>
                                                @endif
                                                @if($siswa->raport == 'tidak sesuai')
                                                <br>
                                                <div class="form-group">
                                                    <label class="font-weight-bold">Raport</label>
                                                    <input type="file" class="form-control" name="raport">
                                                </div>
                                                @endif
                                                @if($siswa->pas_photo == 'tidak sesuai')
                                                <br>
                                                <div class="form-group">
                                                    <label class="font-weight-bold">Pas Photo</label>
                                                    <input type="file" class="form-control" name="pas_photo">
                                                </div>
                                                @endif
                                                
                                                <br>
                                                <button type="submit" class="btn btn-md btn-primary">Input Ulang</button>
                                            @endforeach

                                            </form>
                                        @elseif($user->status == 4)
                                            <span class="text-success">Berkas Sudah Lengkap</span>    
                                        @else
                                            <span class="text-warning">Menunggu Verifikasi</span>
                                        @endif

                                    </td>

                            </table>
                            <hr style="font-weight: bold;">

                        </div>
                    </div>
                    <!--/ Basic Bootstrap Table -->
                    <br>


                    </form>
                    <div class="row">
                        <!-- <div class="col-sm-3"></div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-4">
                            <a href="{{route('siswaInputData')}}"> <button class="btn btn-primary">Ajukan kembali</button></a>
                        </div>
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                            <button type="submit" class="btn btn-primary">Verifikasi</button>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>
@endauth
@endsection