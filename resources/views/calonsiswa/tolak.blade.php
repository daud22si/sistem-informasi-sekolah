@extends('layouts.app-master')

@section('content')
@auth
<main id="main" class="main">

    <div class="pagetitle">
        <h1><b>Data Calon Siswa</b></h1>
        <nav>
        </nav>
    </div>

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Pilih Data Yang Tidak Sesuai</h5>
                            <form action="{{ route('calonsiswa.tolakPerform', $id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div>
                                    <input type="checkbox" name="documents[]" value="kartu_keluarga"> KK
                                </div>
                                <br>
                                <div>
                                    <input type="checkbox" name="documents[]" value="akta_kelahiran"> Akta
                                </div>
                                <br>
                                <div>
                                    <input type="checkbox" name="documents[]" value="ijazah_terakhir"> Ijazah
                                </div>
                                <br>
                                <div>
                                    <input type="checkbox" name="documents[]" value="raport"> Raport
                                </div>
                                <br>
                                <div>
                                    <input type="checkbox" name="documents[]" value="pas_photo"> Pas Photo
                                </div>
                                <br>

                                <button type="submit" class="btn btn-md btn-primary">Konfirmasi</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min. js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap .min.js"></script>
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>

@endauth
@endsection