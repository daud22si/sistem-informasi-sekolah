<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Mahasiswa - Penerimaan Mahasiswa Baru</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.ss">
</head>

<body style="background: lightgray">
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                    <div class="card-body">
                        <a href="{{ route('Mahasiswa.create') }}" class="btn btn-md btn-success mb-3">TAMBAH MAHASISWA</a>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">NAMA MAHASISWA</th>
                                    <th scope="col">TEMPAT LAHIR</th>
                                    <th scope="col">TANGGAL LAHIR</th>
                                    <th scope="col">NO HP</th>
                                    <th scope="col">EMAIL</th>
                                    <th scope="col">AKSI</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data as $mahasiswa)
                                <tr>
                                    <td>{{ $mahasiswa->nama_mahasiswa_2257301031}}</td>
                                    <td>{{ $mahasiswa->tempat_lahir}}</td>
                                    <td>{{$mahasiswa->tanggal_lahir}}</td>
                                    <td>{{ $mahasiswa->no_hp}}</td>
                                    <td>{{$mahasiswa->email}}</td>

                                    <td class="text-center">
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{route('mahasiswa.destroy', $mahasiswa->id) }}" method="post">
                                            <a href="{{route('mahasiswa.edit', $mahasiswa->id) }}" class="btn btn-sm btn-primary">EDIT</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <div class="alert alert-danger">
                                    Data mahasiswa belum Tersedia.
                                </div>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        //message with toastr
        @if(session() -> has('success'))
        toastr.success('{{ session ('success')}}', 'BERHASIL!');
        @elseif(session() -> has('error'))
        toastr.error('{{ session('error')}}', 'GAGAL!');
        @endif
    </script>
</body>
</html>