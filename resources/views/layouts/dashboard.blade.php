@extends('layouts.app-master')

@section('content')
@auth
<div class="pagetitle">
<h1><b>Beranda</b></h1>
<div class="col-lg-11">
    <div class="card">
      <div class="card-header">
        <div class="d-flex align-items-center justify-content-between">
          <h5 class="card-title m-0 me-2">Data Penerimaan Siswa Baru SMP Al-Azhar Syifa Budi Pekanbaru II</h5>
          <div class="user-welcome">
    Welcome, {{ auth()->user()->username }}!
</div>  
          <div class="dropdown">
            <button class="btn p-0" type="button" id="transactionID" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="mdi mdi-dots-vertical mdi-24px"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="transactionID">
              <a class="dropdown-item waves-effect" href="javascript:void(0);">Refresh</a>
              <a class="dropdown-item waves-effect" href="javascript:void(0);">Share</a>
              <a class="dropdown-item waves-effect" href="javascript:void(0);">Update</a>
            </div>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row g-3">
          <div class="col-md-3 col-6">
            <div class="d-flex align-items-center">
              <div class="avatar">
                <div class="avatar-initial bg-primary rounded shadow">
                  <i class="mdi mdi-account-group"></i>
                </div>
              </div>
              <div class="ms-3">
                <div class="small mb-1">Calon Siswa</div>
                <h5 class="mb-0">{{ $totalcalonsiswa }}</h5>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-6">
            <div class="d-flex align-items-center">
              <div class="avatar">
                <div class="avatar-initial bg-success rounded shadow">
                  <i class="mdi mdi-border-color"></i>
                </div>
              </div>
              <div class="ms-3">
                <div class="small mb-1">Soal</div>
                <h5 class="mb-0">{{ $totalsoal }}</h5>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-6">
            <div class="d-flex align-items-center">
              <div class="avatar">
                <div class="avatar-initial bg-warning rounded shadow">
                  <i class="mdi mdi-check-circle-outline"></i>
                </div>
              </div>
              <div class="ms-3">
                <div class="small mb-1">Seleksi</div>
                <h5 class="mb-0">{{ $totalseleksi }}</h5>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-6">
            <div class="d-flex align-items-center">
              <div class="avatar">
                <div class="avatar-initial bg-info rounded shadow">
                  <i class="mdi mdi-account"></i>
                </div>
              </div>
              <div class="ms-3">
                <div class="small mb-1">Panitia PSB</div>
                <h5 class="mb-0">{{ $totalpanitiapsb }}</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endauth
@endsection