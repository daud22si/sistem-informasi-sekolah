<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        $totalcalonsiswa = DB::table('calon_siswa')->count();
        $totalsoal = DB::table('soal')->count();
        $totalpanitiapsb = DB::table('panitia_psb')->count();
        $totalseleksi = DB::table('seleksi')->count();

        // Pass all variables in a single array
        return view('layouts.dashboard', [
            'totalcalonsiswa' => $totalcalonsiswa,
            'totalsoal' => $totalsoal,
            'totalpanitiapsb' => $totalpanitiapsb,
            'totalseleksi' => $totalseleksi,
    
        ]);
    }
}