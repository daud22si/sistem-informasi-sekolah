<?php

namespace App\Http\Controllers;

use App\Models\AkunSiswa;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\User;


class AkunSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('siswa.index');
    }
    public function seleksi()
    {
        return view('siswa.seleksi');
    }
    public function hasilSeleksi()
    {
        $user = Auth::user();
        return view('siswa.hasilSeleksi', compact('user'));
    }
    public function verifikasiForm()
    {
        $guard = Auth::guard(); 
        $user = $guard->user(); 
        $userID = $user->id; 
        
        $data = DB::select(DB::raw("select * from calon_siswa where id_calon_siswa ='$userID'"));
        // $kartuKeluarga = $data[0]->kartu_keluarga;
        // dd($data);
        // echo $kartuKeluarga;
        return view('siswa.verifikasi', compact('user', 'data'));
        
    }
    public function formPendaftaraan()
    {
        $user = Auth::user();
        if ($user->status > 1) {
            Session::flash('alert', 'Anda sudah melakukan pendaftaran, mohon kehalaman verifikasi!');
            return redirect()->route('homeSiswa');
        } else  {
            return view('siswa.inputData', compact('user'));
        }
    }
    public function tesAkademik()
    {

        $user = Auth::user();

        if ($user->statusTesAkademik > 1) {
            Session::flash('alert', 'Anda sudah melakukan tes ini!');
            return redirect()->route('homeSiswa');
        } else  {
            $data = DB::select(DB::raw("select * from soal where kategori_soal ='akademik'"));
            return view('siswa.tesAkademik', compact('data'));
        }
    
    }

    public function submit(Request $request)
    {
        $user = Auth::user();

        $jawabanSiswa = $request->input('jawaban');
        $soalIds = $request->input('soal_id');

        
        // Ambil kunci jawaban dari database
        $kunciJawaban = $this->getKunciJawaban($soalIds);

       
        // Hitung jumlah jawaban yang benar
        $jumlahBenar = $this->hitungJawabanBenar($jawabanSiswa, $kunciJawaban);

        // Contoh sederhana: Jika lebih dari setengah jawaban benar, lulus
        if ($jumlahBenar >= count($jawabanSiswa) / 2) {
            $guard = Auth::guard(); 
            $user = $guard->user(); 
            $userID = $user->id; 
            $newStatus = 3;
            $userToUpdate = User::find($userID);
            $userToUpdate->update(['statusTesAkademik' => $newStatus]);
            if ($userToUpdate){
                Session::flash('alert', 'Anda telah menyelesaikan tes!');
                return redirect()->route('homeSiswa');
            }
            
        } else {
            $guard = Auth::guard(); 
            $user = $guard->user(); 
            $userID = $user->id; 
            $newStatus = 2;
            $userToUpdate = User::find($userID);
            $userToUpdate->update(['statusTesAkademik' => $newStatus]);
            if ($userToUpdate){
                Session::flash('alert', 'Anda telah menyelesaikan tes!');
                return redirect()->route('homeSiswa');
            }
        }
        echo "$hasil";
        // return view('siswa.hasilUjian', compact('hasil'));
        
    }

    private function getKunciJawaban($soalIds)
    {
        // Ambil kunci jawaban dari database berdasarkan ID soal
        $kunciJawaban = DB::table('soal')->whereIn('id', $soalIds)->pluck('kunci_jawaban', 'id')->toArray();

        return $kunciJawaban;
    }

    private function hitungJawabanBenar($jawabanSiswa, $kunciJawaban)
    {   
        $jawabanSiswa = array_values($jawabanSiswa);
        $kunciJawaban = array_values($kunciJawaban);

        if (count($jawabanSiswa) == count($kunciJawaban)) {
            // Memastikan jumlah elemen sama
            $jumlahBenar = 0;

            // Membandingkan nilai pada baris yang sama
            foreach ($jawabanSiswa as $index => $jawaban) {
                if ($jawaban == $kunciJawaban[$index]) {
                    $jumlahBenar++;
                }
            }

            $benar = $jumlahBenar;
            // Sekarang $jumlahBenar akan berisi jumlah jawaban yang benar
            return $benar;
        } else {
            echo "Jumlah elemen pada kedua array tidak sama.";
        }
        
        
    }
    public function tesPsikotest()
    {

        $user = Auth::user();

        if ($user->statusTesPsikotest > 1) {
            Session::flash('alert', 'Anda sudah melakukan tes ini!');
            return redirect()->route('homeSiswa');
        } else  {
            $data = DB::select(DB::raw("select * from soal where kategori_soal ='psikotest'"));
            return view('siswa.tesPsikotest', compact('data'));
        }
    
    }

    public function submit2(Request $request)
    {
        $jawabanSiswa = $request->input('jawaban');
        $soalIds = $request->input('soal_id');

        
        // Ambil kunci jawaban dari database
        $kunciJawaban = $this->getKunciJawaban2($soalIds);

       
        // Hitung jumlah jawaban yang benar
        $jumlahBenar = $this->hitungJawabanBenar2($jawabanSiswa, $kunciJawaban);

        // Contoh sederhana: Jika lebih dari setengah jawaban benar, lulus
        if ($jumlahBenar >= count($jawabanSiswa) / 2) {
            $guard = Auth::guard(); 
            $user = $guard->user(); 
            $userID = $user->id; 
            $newStatus = 3;
            $userToUpdate = User::find($userID);
            $userToUpdate->update(['statusTesPsikotest' => $newStatus]);
            if ($userToUpdate){
                Session::flash('alert', 'Anda telah menyelesaikan tes!');
                return redirect()->route('homeSiswa');
            }
        } else {
            $guard = Auth::guard(); 
            $user = $guard->user(); 
            $userID = $user->id; 
            $newStatus = 2;
            $userToUpdate = User::find($userID);
            $userToUpdate->update(['statusTesPsikotest' => $newStatus]);
            if ($userToUpdate){
                Session::flash('alert', 'Anda telah menyelesaikan tes!');
                return redirect()->route('homeSiswa');
            }
        }
        
    }

    private function getKunciJawaban2($soalIds)
    {
        // Ambil kunci jawaban dari database berdasarkan ID soal
        $kunciJawaban = DB::table('soal')->whereIn('id', $soalIds)->pluck('kunci_jawaban', 'id')->toArray();

        return $kunciJawaban;
    }

    private function hitungJawabanBenar2($jawabanSiswa, $kunciJawaban)
    {   
        $jawabanSiswa = array_values($jawabanSiswa);
        $kunciJawaban = array_values($kunciJawaban);

        if (count($jawabanSiswa) == count($kunciJawaban)) {
            // Memastikan jumlah elemen sama
            $jumlahBenar = 0;

            // Membandingkan nilai pada baris yang sama
            foreach ($jawabanSiswa as $index => $jawaban) {
                if ($jawaban == $kunciJawaban[$index]) {
                    $jumlahBenar++;
                }
            }

            $benar = $jumlahBenar;
            // Sekarang $jumlahBenar akan berisi jumlah jawaban yang benar
            return $benar;
        } else {
            echo "Jumlah elemen pada kedua array tidak sama.";
        }
        
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param LoginRequest
     * @return \Illuminate\Http\Response
     * 
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'email' => 'required',
        //     'nisn' => 'required',
        //     'nama_lengkap' => 'required',
        //     'jenis_kelamin' => 'required',
        //     'tempat_tanggal_lahir' => 'required',
        //     'agama' => 'required',
        //     'alamat' => 'required',
        //     'anak_ke' => 'required',
        //     'sekolah_asal' => 'required',
        //     'nama_orangtua' => 'required',
        //     'no_hp_orangtua' => 'required',
        //     'kartu_keluarga' => 'required',
        //     'akta_kelahiran' => 'required',
        //     'ijazah_terakhir' => 'required',
        //     'raport' => 'required',
        //     'pas_photo' => 'required',
        //     'status_penerimaan' => 'required',
        //     'tanggal_penerimaan' => 'required',
        // ]);
        //upload image
        $kartu_keluarga = $request->file('kartu_keluarga');
        $kartu_keluarga->storeAs('public/calonsiswa', $kartu_keluarga->hashName());

        $akta_kelahiran = $request->file('akta_kelahiran');
        $akta_kelahiran->storeAs('public/calonsiswa', $akta_kelahiran->hashName());

        $ijazah_terakhir = $request->file('ijazah_terakhir');
        $ijazah_terakhir->storeAs('public/calonsiswa', $ijazah_terakhir->hashName());

        $raport = $request->file('raport');
        $raport->storeAs('public/calonsiswa', $raport->hashName());

        $pas_photo = $request->file('pas_photo');
        $pas_photo->storeAs('public/calonsiswa', $pas_photo->hashName());


        $guard = Auth::guard(); 
        $user = $guard->user(); 
        $userID = $user->id;
        DB::insert("INSERT INTO `calon_siswa` (`id_calon_siswa`, `email`, `nisn`,`nama_lengkap`,
                                               `jenis_kelamin`,`tempat_tanggal_lahir`,`agama`,`alamat`,`anak_ke`,`sekolah_asal`,
                                               `nama_orangtua`,`no_hp_orangtua`,`kartu_keluarga`,`akta_kelahiran`,`ijazah_terakhir`,
                                               `raport`,`pas_photo`,`tanggal_penerimaan`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            [
                $userID,
                $request->email,
                $request->nisn,
                $request->nama_lengkap,
                $request->jenis_kelamin,
                $request->tempat_tanggal_lahir,
                $request->agama,
                $request->alamat,
                $request->anak_ke,
                $request->sekolah_asal,
                $request->nama_orangtua,
                $request->no_hp_orangtua,
                $kartu_keluarga->hashName(),
                $akta_kelahiran->hashName(),
                $ijazah_terakhir->hashName(),
                $raport->hashName(),
                $pas_photo->hashName(),
                $request->tanggal_penerimaan
            ]
        );
        
        $userToUpdate = User::find($userID);
        $userToUpdate->update(['status' => 2]);

        return redirect()->route('homeSiswa')->with(['success' => 'Data Berhasil Disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AkunSiswa  $akunSiswa
     * @return \Illuminate\Http\Response
     */
    public function show(AkunSiswa $akunSiswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AkunSiswa  $akunSiswa
     * @return \Illuminate\Http\Response
     */
    public function edit(AkunSiswa $akunSiswa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AkunSiswa  $akunSiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateData = [];

    if ($request->hasFile('kartu_keluarga')) {
        $kartu_keluarga = $request->file('kartu_keluarga');
        $kartu_keluarga->storeAs('public/calonsiswa', $kartu_keluarga->hashName());
        $updateData['kartu_keluarga'] = $kartu_keluarga->hashName();
    }

    if ($request->hasFile('akta_kelahiran')) {
        $akta_kelahiran = $request->file('akta_kelahiran');
        $akta_kelahiran->storeAs('public/calonsiswa', $akta_kelahiran->hashName());
        $updateData['akta_kelahiran'] = $akta_kelahiran->hashName();
    }

    if ($request->hasFile('ijazah_terakhir')) {
        $ijazah_terakhir = $request->file('ijazah_terakhir');
        $ijazah_terakhir->storeAs('public/calonsiswa', $ijazah_terakhir->hashName());
        $updateData['ijazah_terakhir'] = $ijazah_terakhir->hashName();
    }

    if ($request->hasFile('raport')) {
        $raport = $request->file('raport');
        $raport->storeAs('public/calonsiswa', $raport->hashName());
        $updateData['raport'] = $raport->hashName();
    }

    if ($request->hasFile('pas_photo')) {
        $pas_photo = $request->file('pas_photo');
        $pas_photo->storeAs('public/calonsiswa', $pas_photo->hashName());
        $updateData['pas_photo'] = $pas_photo->hashName();
    }

    // Lakukan hal serupa untuk kolom-kolom lain yang mungkin Anda miliki

    DB::table('calon_siswa')->where('id_calon_siswa', $id)->update($updateData);

    $userToUpdate = User::find($id);
    $userToUpdate->update(['status' => 2]);
    DB::table('calon_siswa')->where('id_calon_siswa', $id)->update(['status' => '2']);
    return redirect()->route('homeSiswa');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AkunSiswa  $akunSiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(AkunSiswa $akunSiswa)
    {
        //
    }
}
