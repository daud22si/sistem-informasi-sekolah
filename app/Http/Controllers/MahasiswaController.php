<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select(DB::raw("select * from Mahasiswa"));
        return view('Mahasiswa.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Mahasiswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_mahasiswa_2257301031' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'no_hp' => 'required',
            'email' => 'required'
        ]);

        DB::insert(
            "INSERT INTO `Mahasiswa` (`id`, `nama_mahasiswa_2257301031`, `tempat_lahir`, `tanggal_lahir`, `no_hp`, `email`) VALUES (uuid(), ?, ?, ?, ?, ?)",
            [$request->nama_mahasiswa_2257301031, $request->tempat_lahir, $request->tanggal_lahir, $request->no_hp, $request->email]
        );
        return redirect()->route('Mahasiswa.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('Mahasiswa')->where('id', $id)->first();
        return view('Mahasiswa.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_mahasiswa_2257301031' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'no_hp' => 'required',
            'email' => 'required'
        ]);

        DB::update("UPDATE `Mahasiswa` SET `nama_mahasiswa_2257301031`=?,`tempat_lahir`=?,`tanggal_lahir`=?, `no_hp`=?, `email`=? WHERE id=?",
            [$request->nama_mahasiswa_2257301031, $request->tempat_lahir, $request->tanggal_lahir, $request->no_hp, $request->email, $id]);
        return redirect()->route('Mahasiswa.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('Mahasiswa')->where('id', $id)->delete();
        //redirect to index
        return redirect()->route('Mahasiswa.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}