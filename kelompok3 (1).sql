-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2024 at 02:12 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kelompok3`
--

-- --------------------------------------------------------

--
-- Table structure for table `calon_siswa`
--

CREATE TABLE `calon_siswa` (
  `id_calon_siswa` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nisn` int(25) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `tempat_tanggal_lahir` varchar(255) NOT NULL,
  `agama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `anak_ke` varchar(255) NOT NULL,
  `sekolah_asal` varchar(255) NOT NULL,
  `nama_orangtua` varchar(255) NOT NULL,
  `no_hp_orangtua` varchar(255) NOT NULL,
  `kartu_keluarga` varchar(255) NOT NULL,
  `akta_kelahiran` varchar(255) NOT NULL,
  `ijazah_terakhir` varchar(255) NOT NULL,
  `raport` varchar(255) NOT NULL,
  `pas_photo` varchar(255) NOT NULL,
  `status_penerimaan` varchar(255) NOT NULL,
  `tanggal_penerimaan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `calon_siswa`
--

INSERT INTO `calon_siswa` (`id_calon_siswa`, `email`, `nisn`, `nama_lengkap`, `jenis_kelamin`, `tempat_tanggal_lahir`, `agama`, `alamat`, `anak_ke`, `sekolah_asal`, `nama_orangtua`, `no_hp_orangtua`, `kartu_keluarga`, `akta_kelahiran`, `ijazah_terakhir`, `raport`, `pas_photo`, `status_penerimaan`, `tanggal_penerimaan`) VALUES
('1f29450b-add1-11ee-95b5-0a0027000007', 'denisa@gmail.com', 1233, 'denisaruth', 'perempuan', 'denisa', 'sbhsguf', 'iuhuih', '1', 'bsjfhu', 'bvdeh', '0162489126', '6GAxV7awptlf9LJuh9zlcf6c76EVzFMOQO0z6rVt.png', 'O2r80QIkAamaEGxivZ3j7PJhw9iYPzl5uCndkYA7.png', 'OA7Nvox9CQ8jslT5dnE50ToJuCQvJVcivf54heY5.png', 'a9mGSK0vFJm5wUkocu265oBYSm97d7NO4vAWoDab.png', 'X21oPnfZ8xGvTMhovK3PfMGSNOCgUuCcsHcrHbMH.png', 'jbuig', '2024-01-08');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `panitia_psb`
--

CREATE TABLE `panitia_psb` (
  `id_panitia_psb` varchar(255) NOT NULL,
  `email_panitia_psb` varchar(255) NOT NULL,
  `nama_panitia_psb` varchar(255) NOT NULL,
  `password_panitia_psb` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seleksi`
--

CREATE TABLE `seleksi` (
  `id_seleksi` varchar(255) NOT NULL,
  `jenis_seleksi` varchar(255) NOT NULL,
  `tanggal_seleksi` date NOT NULL,
  `jam_seleksi` time NOT NULL,
  `tempat_seleksi` varchar(255) NOT NULL,
  `hasil_seleksi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `seleksi`
--

INSERT INTO `seleksi` (`id_seleksi`, `jenis_seleksi`, `tanggal_seleksi`, `jam_seleksi`, `tempat_seleksi`, `hasil_seleksi`) VALUES
('01baa9d2-ad5f-11ee-9f3a-0a0027000007', 'test', '2024-01-07', '20:16:00', 'GOR', 'lulus');

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE `soal` (
  `id` varchar(255) NOT NULL,
  `kategori_soal` varchar(255) NOT NULL,
  `materi_soal` varchar(255) NOT NULL,
  `deskripsi_soal` varchar(255) NOT NULL,
  `kunci_jawaban` varchar(255) NOT NULL,
  `skor_maksimal` int(25) NOT NULL,
  `gambar_soal` varchar(255) NOT NULL,
  `waktu_pengerjaan` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `soal`
--

INSERT INTO `soal` (`id`, `kategori_soal`, `materi_soal`, `deskripsi_soal`, `kunci_jawaban`, `skor_maksimal`, `gambar_soal`, `waktu_pengerjaan`) VALUES
('511154ca-ad5f-11ee-9f3a-0a0027000007', 'sulit', 'mtk', 'hitunglah', '1234', 100, 'fzEQkgGJSo81ht7rY22pCs170wPMdqafrPpCj6Wx.png', '20:18:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, NULL, 'denisa22si@mahasiswa.pcr.ac.id', 'denisa123', NULL, '$2y$10$QCv6CN1M0PAH.ZuEqIHznuSD94FETre6dgoczbawWTpUXsbD/4N36', NULL, '2023-12-01 04:50:36', '2023-12-01 04:50:36'),
(3, NULL, 'lutfi22si@mahasiswa.pcr.ac.id', 'lutfi234', NULL, '$2y$10$6WQJtr2IJ7haTOV4HoThVeQEzOusVm.QoWcphly1uN.oQc32pZk7q', NULL, '2023-12-06 02:29:52', '2023-12-06 02:29:52'),
(4, NULL, 'denisaruthdiani@gmail.com', 'denisa', NULL, '$2y$10$6SInGqhjrk348Ip1cZaJweUmU5LpF5AELpL07BF8NHydZSweYXe1i', NULL, '2024-01-07 03:00:52', '2024-01-07 03:00:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `calon_siswa`
--
ALTER TABLE `calon_siswa`
  ADD PRIMARY KEY (`id_calon_siswa`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panitia_psb`
--
ALTER TABLE `panitia_psb`
  ADD PRIMARY KEY (`id_panitia_psb`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `seleksi`
--
ALTER TABLE `seleksi`
  ADD PRIMARY KEY (`id_seleksi`);

--
-- Indexes for table `soal`
--
ALTER TABLE `soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
